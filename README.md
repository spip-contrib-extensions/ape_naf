# Codes APE ou NAF

Ce plugin offre des outils pour traiter les codes APE ou NAF. Une démonstration est disponible.

Il propose notamment des mécanismes de vérifications automatiques et d'affichages conditionnels.

# API propre au plugin

Une API offre diverses fonctions :

1. `creer_ape_naf` permet de vérifier qu'un fichier json constitutif du répertoire des APE ou NAF est présent et actualisé ; permet de le créer si nécessaire.
2. `lire_ape_naf` permet de vérifier si la valeur proposée comme un code NAF ou API est bien dans le répertoire. Permet d'avoir la correspondance du code en un libellé intelligible. Permet d'avoir un tableau code libelle constitutif de l'ensemble du répertoire.

# Utilisation du plugin Vérifier

Une vérification devient accessible si l'on rend actif le plugin Vérifier. Cette vérification du code `ape_naf` connait des options :
1.  `normaliser`: permet de normaliser la valeur soumis sous réserve qu'elle soit valide.
2.  `repertorier`: permet de vérifier au delà de la cohérence de la valeur en vérifiant l'existance de la valeur dans le répertoire des codes APE ou NAF. Cette option a pour défaut la valeur oui.

# Utilisation du plugin Saisies pour formulaires

Une saisie supplémentaire, `ape_naf` est proposée pour une utilisation par le plugin Saisies pour formulaires. Elle est constituée des élements suivants :
1. Un *contrôleur* de la saisie en héritage des saisies `input` ou `selection` selon l'utilisation ou non du répertoire des codes et le souhait de saisir de multiple de valeurs ou non ;
2. Une *vue* de la saisie ;
3. Un *constructeur* de la saisie.