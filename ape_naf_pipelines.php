<?php
/**
 * Utilisations de pipelines par le plugin ape_naf
 *
 * @plugin     APE_NAF
 * @copyright  2024
 * @author     Thrax
 * @licence    GNU/GPL
 * @package    SPIP\Ape_naf\Pipelines
 * @link https://programmer.spip.net/-Liste-des-pipelines- Liste des pipelines de SPIP
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Les tâches périodiques ou Cron
 *
 * Ce pipeline permet de déclarer des fonctions exécutées de manière périodique par SPIP. 
 * Il est appelé dans le fichier ecrire/inc/genie.php par la fonction taches_generales, 
 * prend et retourne un tableau associatif ayant :
 * pour clé le nom de la fonction à exécuter (depuis le répertoire genie/)
 * et pour valeur la durée en seconde entre chaque exécution.
 *
 * @param  array $taches_generales
 *         Le contexte du pipeline
 * @return array $taches_generales
 *         Le contexte du pipeline modifié
 */
function ape_naf_taches_generales_cron($taches_generales){
	$taches_generales['ape_naf_actualisation'] = 30 * 24 * 3600; // une fois tous les trente jours
	
	return $taches_generales;
}
