<?php

/**
 * API de vérification : vérification de la validité d'un code APE ou un code NAF
 *
 * Le code APE (pour « code d'activité principale »,
 * ou le code NAF pour « nomenclature d’activité française », 
 * (il s’agit de la même chose),
 * permet d'identifier la branche d'activité principale de l'entreprise
 * et de chacun de ses établissements.
 *
 * @uses plugin verifier
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Validation d'un code APE ou d'un code NAF
 *
 * Composé de 5 caractères (4 chiffres et une lettre)
 *
 * @param string $valeur
 *   La valeur à vérifier.
 * @param array $options
 *   Les options de la vérification : normaliser|repertorier
 *   Par défaut, il y aura une vérification de la valeur dans le répertoire
 * @param null $valeur_normalisee
 *   Si normalisation à faire, la variable sera rempli par le code APE ou NAF normalisé.
 * @return string
 *   Retourne une chaine vide si c'est valide, sinon une chaine expliquant l'erreur.
 */
function verifier_ape_naf_dist($valeur, $options = ['repertorier' => 'oui'], &$valeur_normalisee = null) : string {

	// On supprime les espaces ou les points avant d'effectuer les tests
	// (parfois la division du code, c'est à dire les deux premiers caractères,
	// sont séparés des autres caractères par un point ou un espace).
	$valeur = preg_replace('#\s|\.#', '', $valeur);

	$erreur = _T('ape_naf:erreur_ape_naf');

	// un string
	if (!is_string($valeur)) {
		return $erreur;
	}

	// 5 caractères, 4 chiffres une lettre
	if (!preg_match('/^[0-9]{4}[A-Za-z]$/', $valeur)) {
		return $erreur;
	}

	// un code effectivement dans le répertoire
	if (isset($options['repertorier']) and $options['repertorier'] == 'oui'){
		if ($erreur = verifier_repertorier($valeur, $options)){
			return $erreur;
		}
	}

	if (isset($options['normaliser'])) {
		// des lettres majuscules
		$valeur_normalisee = strtoupper($valeur);
	}
	return '';
}

/**
 * Fonction privée pour vérifier que le code est bien répertorié
 *
 * @param string   $valeur  La valeur à vérifier.
 * @return string  vide si ok, message d'erreur sinon.
 **/
function verifier_repertorier(string $valeur) : string {
	include_spip('inc/ape_naf');
	// vérifier la valeur dans le répertoire
	return lire_ape_naf($valeur);
}