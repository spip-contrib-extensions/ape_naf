# Plugin ape_naf

## [Unreleased]

## [1.1.2] 2024-10-15

- Commit 0e5d124f : fix #4 - le fichier json des codes est placé dans le dossier local, comme toutes les ressources temporaires accessibles au public.
- Commit 94f16368 : fix #5 - avoir un cron (mensuel) qui regarde s'il est nécessaire de renouveler les codes

## [1.1.1] 2024-10-14

### CHANGED
- Commit fb1ba212 : mise en conformité des fichiers de langue : adopter la syntaxe valide à partir de SPIP 4.1 ce qui facilitera la migration en SPIP 5
- Commit 5f0938e6 et 5bbd69a3 : intégration d'une démo et compatibilité SPIP 5.0.*