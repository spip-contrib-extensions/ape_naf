<?php

/**
 * Tâche périodique
 *
 * @link https://programmer.spip.net/Fonctionnement-du-cron
 * @link https://contrib.spip.net/Valeurs-de-retour-d-un-cron-fonction-pour-taches
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Actualiser la liste des codes APE ou codes NAF
 *
 * @uses creer_ape_naf() dans inc/ape_naf.php
 *
 */
function genie_ape_naf_actualisation_dist($t){
	include_spip('inc/ape_naf');
	// réactualiser la liste des codes
	$resultat = creer_ape_naf(100);
	switch ($resultat) {
		case -2:
			spip_log('Le fichier ape_naf n’a pas pu être réactualisé', 'genie.' . _LOG_ERREUR);
		break;
		case -1:
			spip_log('Le fichier ape_naf est réactualisé', 'genie.' . _LOG_INFO_IMPORTANTE);
		break;
		default:
			spip_log('Le fichier ape_naf n’a pas besoin d’être réactualisé', 'genie.' . _LOG_INFO);
	}
	// retourner l'état de la tâche
	return 1; // la tâche a été traitée
}