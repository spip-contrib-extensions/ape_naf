<?php
 /**
  * Fonctions utiles au plugin
  *
  * @plugin     Ape_nap
  * @copyright  2024
  * @author     Vincent CALLIES
  * @licence    GNU/GPL
  * @package    SPIP\Ape_naf\Fonctions
  */
 
 if (!defined('_ECRIRE_INC_VERSION')) return;