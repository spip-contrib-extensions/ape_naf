<?php
/**
 * Ce fichier contient les fonctions d'API du plugin Code APE ou Code NAF
 *
 * exemples d'utilisation :
 * ```
 * include_spip('inc/ape_naf');
 * // créer la liste des codes
 * $resultat = creer_ape_naf(100);
 * switch ($resultat) {
 *     case -2:
 *         echo "Le fichier n’est pas créé!";
 *         break;
 *     case -1:
 *         echo "Le fichier est créé.";
 *         break;
 *     default:
 *        echo "Le fichier a $resultat jours d'ancienneté et n'a pas besoin d'être réactualisé.";
 * }
 * // avoir le libellé du code 0520Z
 * print_r(lire_ape_naf('0520Z',['libelle' => '']));
 * // avoir tous les codes et libellés
 * print_r(lire_ape_naf('*',['libelle' => 'avec_code']));
 * ```
 *
 * @package SPIP\Ape_naf\Api
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Avoir un répertoire des codes APE et NAF à jour
 *
 * La fonction renouvelle le répertoire des codes APE et NAF
 * ou le crée s’il est inexistant.
 * La documentation technique des services d'accès aux données 
 * exposés par la plateforme d'Opendata du Grand lyon 
 * se trouve à l'adresse suivante : https://rdata-grandlyon.readthedocs.io.
 * Nous utilisons ici le Services REST (en JSON)
 * https://data.grandlyon.com/fr/datapusher/ws/rdata
 *
 * @param int  $jours jour d'ancienneté du fichier avant obsolescence
 * @return int -1 création du fichier
 *             -2 erreur
 *             x  Pas de création. Nombre du jours restant avant le renouvellement du fichier
 */
function creer_ape_naf(int $jours) : int {
	$retour = -1;
	$repertoire = _DIR_VAR . 'cache-json/';
	$fichier = $repertoire . 'ape_naf.json';
	// Le json a vocation à être mise à jour indépendamment du plugin, 
	// il est stocké dans le dossier local,
	// comme toutes les ressources temporaires accessibles au public.
	if (!file_exists($repertoire)) { 
		// Créer le dossier s'il n'existe pas
		// 0644 : Tous les droits pour le propriétaire, lire pour les autres
		if (!mkdir($repertoire, 0744)){
			spip_log("Le répertoire $repertoire n’a pas pu être créé !", 'ape_naf.' . _LOG_ERREUR);
			return -2;
		}
	} elseif (file_exists($fichier)) {
		// si le fichier existe, calculer depuis combien de jours
		$date_fichier = filemtime($fichier);
		$date_fichier = new DateTime(date("Y-m-d",filemtime($fichier)));
		$date = new DateTime("now");
		$interval = $date_fichier->diff($date);
		$retour = $interval->format('%R%a');
	}
	// créer ou renouveller le fichier si nécessaire
	if ($retour < 0)
	{
		$url ='https://data.grandlyon.com/fr/datapusher/ws/rdata/insee.codenaf/all.json?maxfeatures=1000&start=1';
		$ch = curl_init($url);
		// Ouvre en écriture seule
		$fp = fopen($fichier, "w");

		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);

		curl_exec($ch);
		if(curl_error($ch)) {
		    fwrite($fp, curl_error($ch));
		}
		curl_close($ch);
		fclose($fp);
		return $retour;
	}
	return $retour;
}

/**
 * Lire le répertoire à la recherche d'une valeur ou des valeurs
 *
 * @param string  $valeurs code APE ou NAF ou encore '*'
 * @param array   $options libelle donne le libellé du code
 *                         libelle `avec_code` donne le code suivi du libelle
 * @return array|string  tableau constitutif des valeurs du répertoire
 *                       valeur du libellé du code
 *                       valeur vide
 *                       message d'erreur
 */
function lire_ape_naf(string $valeur, ?array $options = []) : array|string {

	static $data = [];

	// mettre le contenu du repertoire dans la variable
	if (!$data){
		$fichier = _DIR_VAR . 'cache-json/' . 'ape_naf.json';
		if ( $json = file_get_contents($fichier) ) {
			if (
				$data = json_decode($json, TRUE)
				and $data != NULL
			){
				unset($json);
			} else {
				return _T('ape_naf:erreur_format');
			}
		} else {
			return _T('ape_naf:erreur_fichier');
		}
	}

	// -- retourner tout le répertoire
	if ($valeur === '*'){
		$retour = [];
		foreach ($data['values'] as $ligne => $valeurs) {
			$cle = substr($valeurs['code'], 0, 2) . substr($valeurs['code'], 3);
			if (isset($options['libelle'])){
				if ($options['libelle'] == 'avec_code'){
					$retour[$cle] = $valeurs['code'] . ' - ' . $valeurs['libelle'];
				} else {
					$retour[$cle] = $valeurs['libelle'];
				}
			} else {
				$retour[] = $cle;
			}
		}
		return $retour;
	}

	// -- vérifier l'existance de la valeur
	// placer un point après la division
	$ape = substr($valeur, 0, 2) . '.' . substr($valeur, 2);
	// accéder à l'élément recherché
	foreach ($data['values'] as $cle => $valeurs) {
		if ($ape == $valeurs['code']){
			// l'élément est trouvé
			if (isset($options['libelle'])){
				if ($options['libelle'] == 'avec_code'){
					return $valeurs['code'] . ' - ' . $valeurs['libelle'];
				}
				return $valeurs['libelle'];
			} else {
				return '';
			}
		}
	}
	// l'élément n'est pas trouvé
	return _T('ape_naf:erreur_non_repertorie');
}