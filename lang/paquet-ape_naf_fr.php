<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'ape_naf_description' => 'Outils pour faciliter la gestion des codes APE ou NAF.',
	'ape_naf_nom' => 'Code APE ou Code NAF',
	'ape_naf_slogan' => 'Gérer facilement les codes APE ou NAF',
];