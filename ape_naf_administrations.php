<?php

/**
 * Fichier gérant l'installation et désinstallation du plugin ape_naf
 *
 * @plugin     ape_naf
 * @copyright  2024
 * @author     Thrax
 * @licence    GNU/GPL
 * @package    SPIP\Ape_naf\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Fonction d'installation et de mise à jour du plugin .
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/

function ape_naf_upgrade($nom_meta_base_version, $version_cible){
	$maj = [];

	$maj['create'] = [
		['ape_naf_api']
	];
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

function ape_naf_api() {
	include_spip('inc/ape_naf');
	// créer la liste des codes
	$resultat = creer_ape_naf(100);
	switch ($resultat) {
		case -2:
			// Le fichier n’est pas créé!
			spip_log('Le fichier json des codes NAF n’a pas pu être créé.', 'maj.' . _LOG_ERREUR);
	
		break;
		case -1:
			// Le fichier est créé.
			spip_log('Le fichier json des codes NAF est créé.', 'maj.' . _LOG_AVERTISSEMENT);
		break;
		default:
			// Le fichier a $resultat jours d'ancienneté et n'a pas besoin d'être réactualisé
			spip_log('Le fichier json des codes NAF est déjà présent.', 'maj.' . _LOG_AVERTISSEMENT);
	}
}

/**
 * Fonction de désinstallation du plugin.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function ape_naf_vider_tables($nom_meta_base_version) {
	// Il n'est pas nécessaire de détruire le fichier json.
	// c'est une ressource temporaire accessible au public,
	// indépendante du plugin.
	effacer_meta($nom_meta_base_version);
}